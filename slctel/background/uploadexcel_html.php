<?php
header("content-type:text/html;charset=utf-8");

session_start();

if(isset($_SESSION["name"]))

{
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>上传文件</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="blogsystemcss.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="http://cdn.bootcss.com/html5shiv/3.7.0/html5shiv.min.js"></script>
        <script src="http://cdn.bootcss.com/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<style>
		.page-header
		{
		margin-top:0;
		background:#B0E0E6;
		}
		.col-1
		{
		background:#D6D6D6;
		}
		h5
		{
		margin-bottom:0;
		}
	</style>
	</head>
  <body>
	<div class="container">
		<nav class="navbar navbar-default page-header">
			<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><b>大乐透代金券中奖查询</b></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">				
					<ul class="nav navbar-nav navbar-right">
						<li><a href="login.html">退出</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
		</nav>
		<div class="row">
			<div class="col-md-2 col-sm-3">
				<nav class="navbar navbar-default col-1" id="sidebar-wrapper" role="navigation">
					<ul class="nav sidebar-nav">
						<li class="sidebar-brand"> <a href="info.php"><p align="center">领取信息</p></a> </li>
						<li> <a href="slctel.html"> <i class="fa fa-fw fa-home"> </i><p align="center">查&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;询</p></a> </li>
						<li> <a href="uploadexcel.html"><p align="center">上传文件</p></a> </li>
					</ul>
				</nav>
			</div>
			<div class="col-md-10 col-sm-9">
				<form class="form-horizontal" name="form1" method="post" action="uploadexcel.php" enctype="multipart/form-data">
					<div class="form-group">
						<label for="telephone" class="col-sm-2 control-label">导入Excel表：</label>
						<div class="col-sm-8">
							<input type="file" name="file">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" id="button2" name="submit2" class="btn btn-default">上传</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<h5 align="center">技术支持 舜网（<a href="http://www.e23.cn">www.e23.cn</a>）
	</h5>




  </body>
</html>
<?php
}
else
{
	echo '<script>alert("请先登录");location.href="login.php";</script>';
}
?>