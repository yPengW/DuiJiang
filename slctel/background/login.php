<?php
header("content-type:text/html; charset=utf-8");
if(isset($_POST['submit']))
{

$account = $_POST['account'];
$pwd = $_POST['pwd'];

session_start();

$_SESSION["name"]=$account;

isset($session_id)?session_id($session_id):$session_id=session_id();							//设置session

setcookie('session_id',$session_id,time()+1200);

//检查账号密码是否正确

if($account == "admin" && $pwd == "admin")
{
	echo '<script>location.href="info.php";</script>';
	
}
if($account != "admin" || $pwd != "admin")
{
	echo '<script>alert("账号密码输出错误！");location.href="login.html";</script>';
}

}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>登陆</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link rel="stylesheet" href="http://cdn.bootcss.com/twitter-bootstrap/3.0.3/css/bootstrap.min.css">
		<script src="http://cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>  
		<script src="http://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<style>
			.input-group{
			margin:10px 0px;
			}
			h3{
			padding:5px;
			border-bottom:1px solid #ddd;
			}
			li{
			list-style-type:square;
			margin:10px 0;
			}
			em{
			color:#c7254e;
			font-style: inherit;
			background-color: #f9f2f4;
			}
			h5
			{
			margin-top:50px;
			margin-bottom:0;
			}
		</style>
	</head>
	<body>
		<div class="page-header" align="center">
			<h1>大乐透代金券中奖查询后台系统</h1>
		</div>
		<div class="container">
		<div class="row" style="margin-top:30px;">
			<div class="col-md-6" style="border-right:1px solid #ddd;">
				<div class="well col-md-10">
					<h3>用户登录</h3>
					<form action="login.php" method="post">
					<div class="input-group input-group-md">
						<span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user" aria-hidden="true"></i></span>
						<input type="text" class="form-control" id="account" name="account" placeholder="用户名" aria-describedby="sizing-addon1">
					</div>
					<div class="input-group input-group-md">
						<span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock"></i></span>
						<input type="password" class="form-control" id="pwd" name="pwd" placeholder="密码" aria-describedby="sizing-addon1">
					</div>
					<button type="submit" id="submit" name="submit" class="btn btn-success btn-block">
					登录
					</button>
					</form>
				</div>
			</div>
			<div class="col-md-6">
			<h3>
			欢迎使用
			</h3>
			<ul>
				<li>管理员使用<em>账号</em>登录，登录后可以进行操作</li>
				<li>登陆后进行查看领取记录，上传文件！</li>
			</ul>
			</div>
		</div>
		</div>
		
	<h5 align="center">技术支持 舜网（<a href="http://www.e23.cn">www.e23.cn</a>）
	</h5>

	</body>
</html>